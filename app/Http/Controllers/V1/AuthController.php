<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param Request $request #note: Request tidak digunakan di method ini
     * @return void
     */
    public function register(RegisterRequest $request)
    {
        $request->validated(); #note: apakah ini diperlukan? (baca note selanjutnya)
        $name = explode(' ', $request['name'], 2);

        $user = User::create([
            'first_name'    => $name['0'],
            'last_name'     => $name['1'],
            'email'         => $request['email'], #note: ini langsung akses $request, jadi di note sebelumnya tidak perlu ada
            'password'      => $request['password'], #note: bagusnya mengikuti https://laravel.com/docs/10.x/requests#accessing-the-request untuk akses request
        ]);
        // $user = User::create($request->validated());
        return response()->json($user); #note: Gunakan user resource, tapi tanpa address, + gunakan response code yg sesuai
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $accessToken = $user->createToken('authToken');

            return response()->json([
                'token' => $accessToken->plainTextToken,
            ]);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }

    #note: tambahkan endpoint untuk logout
}
