<?php

namespace App\Http\Controllers\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Resources\Api\V1\UserResource;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Get a list of users along with their addresses.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $usersWithAddresses = User::with('addresses')->paginate(10); #note: nama variable tidak perlu menambahkan relasi kecuali dibutuhkan
        return UserResource::collection($usersWithAddresses);
    }

    #note: tidak perlu method search, bisa digabung di method index
    public function search(Request $request)
    {
        // $query = '123 Main Street';
        $query = $request->input('query');

        $usersWithAddresses = User::whereHas('addresses', function ($addressQuery) use ($query) {
            $addressQuery->where('street', 'like', '%' . $query . '%');
        })->with(['addresses' => function ($addressQuery) use ($query) {
            $addressQuery->where('street', 'like', '%' . $query . '%'); #note: tidak perlu filter address
        }])->paginate(10); #note: kasih line space, biar enak bacanya
        return UserResource::collection($usersWithAddresses);
    }

    public function update(LoginRequest $request) #note: nama form request tidak sesuai peruntukannya
    {
        $user = Auth::user();
        $user->update($request->validated());

        return response()->json($user); #note: gunakan empty response
    }

    public function delete(Request $request)
    {
        DB::beginTransaction();
        // $user = Auth::user();
        $user = $request->user();
        // dd($user->loadMissing('addresses'));
        try {
            $deleted = $user->delete();

            if ($deleted) { #note: kenapa ada checking ini?
                DB::commit();
                return response()->json(["message" => "User deleted successfully"]); #note: taruh di luar try catch
            } else {
                DB::rollBack();
                return response()->json(["message" => "User deletion failed"]);
            }
        } catch (\Throwable $th) {
            DB::rollBack();

            return response()->json(["Delete user failed"]); #note: throw saja errornya, jangan kembalikan respon 200
        }
    }

    public function userDetail()
    {
        $user = Auth::user();
        if ($user) { #note: Tidak perlu checking, karena sudah ada verifikasi token
            $userWithAddresses = User::with('addresses')->where('id', $user->id)->first(); #note: pelajari lazy eager loading https://laravel.com/docs/10.x/eloquent-relationships#lazy-eager-loading
            return new UserResource($userWithAddresses);
        } else {
            return response()->json(["message" => "User not found"], 404); #note: Jika ingin mengembalikan error, cara ini tidak salah, tp kurang tepat, pelajar handling error: https://laravel.com/docs/10.x/errors#http-exceptions
        }
    }
}
